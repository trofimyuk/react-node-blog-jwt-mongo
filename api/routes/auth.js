const router = require("express").Router();
const User = require("../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

//REGISTER
router.post("/register", async (req, res) => {
  try {
    // Get user input
    const { username, email, password } = req.body;

    // Validate user input
    if (!(email && password && username)) {
      res.status(400).send("All input is required");
    }

    const oldUser = await User.findOne({ email });

    if (oldUser) {
      return res.status(409).send("User Already Exist. Please Login");
    }

    const salt = await bcrypt.genSalt(10);
    const hashedPass = await bcrypt.hash(req.body.password, salt);
    const newUser = new User({
      username: req.body.username,
      email: req.body.email,
      password: hashedPass,
    });

    const user = await newUser.save();

    res.status(200).json("User created");
  } catch (err) {
    res.status(500).json(err);
  }
});

//LOGIN
router.post("/login", async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    !user && res.status(400).json("Wrong credentials!");

    const validated = await bcrypt.compare(req.body.password, user.password);
    !validated && res.status(400).json("Wrong credentials!");

    const { password, ...others } = user._doc;
    
    // Create token
    const token = jwt.sign(
      { 
        user_id: user._id, 
        email: user.email,
        username: user.username
      },
      process.env.TOKEN_KEY,
      {
        expiresIn: "2h",
      }
    );

    res.status(200).json({
      user: others,
      token
    });
  } catch (err) {
    res.status(500).json(err);
  }
});

//LOGIN
router.post("/loginWithToken", async (req, res) => {
  try {
    let userToken = null;
    let token = null;
    const authHeader = String(req.headers['authorization'] || '');
    if (authHeader.startsWith('Bearer ')) {
      token = authHeader.substring(7, authHeader.length);
    }

    if (!token) {
      return res.status(403).send("Unauthorized");
    }
    try {
      const decoded = jwt.verify(token, process.env.TOKEN_KEY);
      userToken = decoded;
    } catch (err) {
      return res.status(401).send("Invalid Token");
    }

    const user = await User.findOne({ email: userToken.email });
    const { password, ...others } = user._doc;
    
    // Create token
    token = jwt.sign(
      { 
        user_id: user._id, 
        email: user.email,
        username: user.username
      },
      process.env.TOKEN_KEY,
      {
        expiresIn: "2h",
      }
    );

    res.status(200).json({
      user: others,
      token
    });
  } catch (err) {
    res.status(500).json(err);
  }
});

module.exports = router;

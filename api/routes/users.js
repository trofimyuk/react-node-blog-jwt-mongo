const router = require("express").Router();
const User = require("../models/User");
const Post = require("../models/Post");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
// const auth = require("./middleware/auth");


//UPDATE
router.put("/:id", async (req, res) => {
  if (req.body.userId === req.params.id) {
    if (req.body.password) {
      const salt = await bcrypt.genSalt(10);
      req.body.password = await bcrypt.hash(req.body.password, salt);
    }
    try {
      const userForUpdate = {
        ...(req.body.password ? {password: req.body.password}: {}),
        ...(req.body.email ? {email: req.body.email}: {}),
        ...(req.body.username ? {username: req.body.username}: {}),
        ...(req.body.profilePic ? {profilePic: req.body.profilePic}: {}),
      };
      const filter = { _id: req.params.id};
      
      // `doc` is the document _after_ `update` was applied because of
      // `new: true`
      const updatedUser = await User.findOneAndUpdate(filter, userForUpdate, {
        new: true
      });

      const { password, ...others } = updatedUser._doc;
    
      // Create token
      const token = jwt.sign(
        { 
          user_id: updatedUser._id, 
          email: updatedUser.email,
          username: updatedUser.username
        },
        process.env.TOKEN_KEY,
        {
          expiresIn: "2h",
        }
      );

      res.status(200).json({
        user: others,
        token
      });

    } catch (err) {
      res.status(500).json(err);
    }
  } else {
    res.status(401).json("You can update only your account!");
  }
});

//DELETE
router.delete("/:id", async (req, res) => {
  if (req.body.userId === req.params.id) {
    try {
      const user = await User.findById(req.params.id);
      try {
        await Post.deleteMany({ username: user.username });
        await User.findByIdAndDelete(req.params.id);
        res.status(200).json("User has been deleted...");
      } catch (err) {
        res.status(500).json(err);
      }
    } catch (err) {
      res.status(404).json("User not found!");
    }
  } else {
    res.status(401).json("You can delete only your account!");
  }
});

//GET USER
router.get("/:id", async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    const { password, ...others } = user._doc;
    res.status(200).json(others);
  } catch (err) {
    res.status(500).json(err);
  }
});

module.exports = router;

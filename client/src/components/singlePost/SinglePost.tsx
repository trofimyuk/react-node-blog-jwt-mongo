import { useContext, useEffect, useState } from "react";
import { useLocation } from "react-router";
import { Link } from "react-router-dom";
import { Context } from "../../context/Context";
import Api from "../../api";
import {Remove, Edit} from "@mui/icons-material";
import "./singlePost.css";
import React from "react";
import { IconButton, TextField } from "@mui/material";

export default function SinglePost() {
  const location = useLocation();
  const path = location.pathname.split("/")[2];
  const [post, setPost] = useState({}) as any;
  const PF = "http://localhost:5000/images/";
  const { user } = useContext(Context);
  const [title, setTitle] = useState("");
  const [desc, setDesc] = useState("");
  const [updateMode, setUpdateMode] = useState(false);

  useEffect(() => {
    const getPost = async () => {
      const res = await Api.Posts.find(path);
      setPost(res.data);
      setTitle(res.data.title);
      setDesc(res.data.desc);
    };
    getPost();
  }, [path]);

  const handleDelete = async () => {
    try {
      await Api.Posts.delete({post, user});
      window.location.replace("/");
    } catch (err) {}
  };

  const handleUpdate = async () => {
    try {
      await Api.Posts.put({post, user, title, desc});
      setUpdateMode(false)
    } catch (err) {}
  };

  return (
    <div className="singlePost">
      <div className="singlePostWrapper">
        {post.photo && (
          <img src={PF + post.photo} alt="" className="singlePostImg" />
        )}
        {updateMode ? (
          <TextField
            margin="normal"
            required
            fullWidth
            id="title"
            label="Title"
            placeholder="Title"
            name="title"
            autoFocus
            value={title}
            onChange={e=>setTitle(e.target.value)}
          />
        ) : (
          <h1 className="singlePostTitle">
            {title}
            {post.username === user?.username && (
              <div className="singlePostEdit">
                <IconButton color="primary" title="Edit" aria-label="edit" component="span" onClick={() => setUpdateMode(true)}>
                  <Edit />
                </IconButton>
                <IconButton color="primary" title="Remove" aria-label="remove" component="span" onClick={handleDelete}>
                  <Remove />
                </IconButton>
              </div>
            )}
          </h1>
        )}
        <div className="singlePostInfo">
          <span className="singlePostAuthor">
            Author:
            <Link to={`/?user=${post.username}`} className="link">
              <b> {post.username}</b>
            </Link>
          </span>
          <span className="singlePostDate">Created:{` ${new Date(post.createdAt).toDateString()}`}</span>
        </div>
        {updateMode ? (
           <TextField
           margin="normal"
           required
           fullWidth
           id="desc"
           label="Description"
           placeholder="Tell your story..."
           name="desc"
           autoFocus
           value={desc}
           minRows={3}
           multiline
           onChange={(e)=>setDesc(e.target.value)}
         /> 
        ) : (
          <p className="singlePostDesc">{desc}</p>
        )}
        {updateMode && (
          <button className="singlePostButton" onClick={handleUpdate}>
            Update
          </button>
        )}
      </div>
    </div>
  );
}

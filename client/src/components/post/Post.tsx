import "./post.css";
import { Link } from "react-router-dom";
import React from "react";
import { Card, CardActionArea, CardContent, CardMedia, Typography } from "@mui/material";

export default function Post({ post }: any) {
  const PF = "http://localhost:5000/images/";
  return (
    <Card sx={{ maxWidth: '100%' }}>
      <CardActionArea component={Link} to={`/post/${post._id}`}>
        <CardMedia
          component="img"
          height="140"
          image={PF + post.photo} 
          alt={post.title}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {post.title}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {post.desc}
          </Typography>
          <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
            {new Date(post.createdAt).toDateString()}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}

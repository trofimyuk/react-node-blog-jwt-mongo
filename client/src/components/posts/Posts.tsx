import { Grid } from "@mui/material";
import React from "react";
import Post from "../post/Post";
import "./posts.css";

export default function Posts({ posts }: any) {
  return (
    <div className="posts">
      <Grid container spacing={2}>
        {posts.map((p: any,i: any) => (
          <Grid item xs={12} sm={4} md={4} key={i}>
              <Post post={p}/>
          </Grid>
        ))}
      </Grid>
    </div>
  );
}

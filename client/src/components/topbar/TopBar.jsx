import { useContext } from "react";
import { Link } from "react-router-dom";
import { Context } from "../../context/Context";
import "./topbar.css";

export default function TopBar() {
  const { user, dispatch, isAuth } = useContext(Context);
  const PF = "http://localhost:5000/images/"

  const handleLogout = () => {
    dispatch({ type: "LOGOUT" });
  };
  return (
    <div className="top">
      <div className="topCenter">
        <ul className="topList">
          {
            isAuth &&
            <>
              <li className="topListItem">
                <Link className="link" to="/">
                  HOME
                </Link>
              </li>
              <li className="topListItem">
                <Link className="link" to="/write">
                  CREATE
                </Link>
              </li>
              <li className="topListItem" onClick={handleLogout}>
                {user && "LOGOUT"}
              </li>
            </>
          }
        </ul>
      </div>
      <div className="topRight">
        {isAuth ? (
          <Link to="/settings" className="avatar">
            {
              user.profilePic && <img className="topImg" src={PF+user.profilePic} alt="" />
            }
          </Link>
        ) : (
          <ul className="topList">
            <li className="topListItem">
              <Link className="link" to="/login">
                LOGIN
              </Link>
            </li>
            <li className="topListItem">
              <Link className="link" to="/register">
                REGISTER
              </Link>
            </li>
          </ul>
        )}
      </div>
    </div>
  );
}

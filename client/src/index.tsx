import React from "react";
import { createRoot } from 'react-dom/client';
import { ThemeProvider } from '@mui/material/styles';
import App from "./App";
import { ContextProvider } from "./context/Context";
import theme from './theme';
import { CssBaseline } from "@mui/material";

const rootElement = document.getElementById('root');
const root = createRoot(rootElement!);

root.render(
  <ThemeProvider theme={theme}>
    {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
    <CssBaseline />
    <ContextProvider>
      <App />
    </ContextProvider>
  </ThemeProvider>,
);
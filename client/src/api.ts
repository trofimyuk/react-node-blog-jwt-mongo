/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable id-denylist */
/* eslint-disable max-lines */
/* eslint-disable prefer-object-spread, max-len, max-statements, max-lines-per-function, no-param-reassign */
import axios, { AxiosInstance, Canceler, AxiosRequestConfig } from 'axios';

const setBearerAuthToken = (requestConfig: AxiosRequestConfig): AxiosRequestConfig => {
  const authToken = sessionStorage.getItem('token');
  if (authToken && requestConfig.headers) {
    requestConfig.headers.Authorization = `Bearer ${authToken}`;
  }
  return requestConfig;
};

const apiBaseUrl = 'http://localhost:5000/api/';

type GetAxiosResult =
  | AxiosInstance
  | {
      axiosInstance: AxiosInstance;
      cancelFunc: Canceler;
    };

export const getAxiosFunction = function getAxiosInstance(): GetAxiosResult {
  let axiosInstance;
  axiosInstance = axios.create({
    baseURL: apiBaseUrl,
  });

  axiosInstance.interceptors.request.use(setBearerAuthToken);
  return axiosInstance;
};

type ApiClient = Record<string, Record<string, (...args: any[]) => Promise<any>>>;

const getApi = (
  getAxios: () => AxiosInstance = getAxiosFunction as () => AxiosInstance,
): ApiClient => {
  const Api: ApiClient = {};

  Api.Auth = {
    registration({
      username,
      email,
      password,
    }): any {
      return getAxios().post("/auth/register", {
        username,
        email,
        password,
      });
    },
    login({email, password}): any {
      return getAxios().post("/auth/login", {
        email,
        password,
      });
    },
    loginWithToken(): any {
      return getAxios().post("/auth/loginWithToken");
    },
    logout(): any {
      return getAxios().post('api/auth/logout');
    },
    isAuth(): any {
      return getAxios().get('api/auth/isauth');
    },
  };

  Api.Categories = {
    get(): any {
      return getAxios().get("/categories");
    },
  }

  Api.Files = {
    upload(data): any {
      return getAxios().post("/upload", data);
    },
  }

  Api.Users = {
    put({user, updatedUser}): any {
      return getAxios().put("/users/" + user._id, updatedUser);
    },
  }
  
  Api.Posts = {
    // Get
    find(path): any {
      return getAxios().get("/posts/" + path);
    },
    //Delete
    delete({post, user}): any {
      return getAxios().delete(`/posts/${post._id}`, {
        data: { username: user.username },
      });
    },
    //Update
    put({
      post, user, title, desc
    }): any {
      return getAxios().put(`/posts/${post._id}`, {
        username: user.username,
        title,
        desc,
      });
    },
    //Create
    post(post): any {
      return getAxios().post("/posts", post);
    },
    
  };

  return Api;
};

export default getApi();

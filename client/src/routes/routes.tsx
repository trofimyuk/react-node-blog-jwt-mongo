import React from "react";
import {
    BrowserRouter,
    Route,
    Routes,
    Navigate  } from "react-router-dom";
import TopBar from "../components/topbar/TopBar";
import Home from "../pages/home/Home.lazy";
import Login from "../pages/login/Login";
    
import Register from "../pages/register/Register";
import Settings from "../pages/settings/Settings";
import Single from "../pages/single/Single";
import Write from "../pages/write/Write";
import RouteGuard from "./RouteGuard";
import RouteGuardAuth from "./RouteGuardAuth";

const Router = () => {
    return (
    <BrowserRouter>
      <TopBar />
      <Routes>
          <Route element={<RouteGuardAuth />}>
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
          </Route>
          <Route element={<RouteGuard />}>
            <Route path="/" element={<Home />} />
            <Route path="/write" element={<Write />} />
            <Route path="/settings" element={<Settings />} />
            <Route path="/post/:postId" element={<Single />} />
          </Route>
          <Route path="*" element={<Navigate to ="/" />}/>
      </Routes>
    </BrowserRouter>
    );
}

export default Router

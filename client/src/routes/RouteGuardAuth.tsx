import React, { useContext } from 'react';
import { Navigate, Outlet } from 'react-router-dom';
import { Context } from "../context/Context";

const RouteGuardAuth = () => {

  const { isAuth } = useContext(Context);

  return !isAuth
    ? <Outlet/>
    : <Navigate to={{ pathname: '/' }} replace />;
  };

export default RouteGuardAuth;
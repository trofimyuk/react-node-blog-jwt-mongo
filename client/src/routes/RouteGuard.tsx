import React, { useContext } from 'react';
import { Navigate, Outlet } from 'react-router-dom';
import { Context } from "../context/Context";

export const RouteGuard = () => {

  const { isAuth } = useContext(Context);
  
  return isAuth 
    ? <Outlet/>
    : <Navigate to={{ pathname: '/login' }} replace />;
};

export default RouteGuard;
import React from "react";
import "./settings.css";
import { useContext, useState } from "react";
import { Context } from "../../context/Context";
import Api from "../../api";
import {Add} from "@mui/icons-material";

import axios from "axios";
import { Box, Button, TextField } from "@mui/material";

interface User{
  userId: string;
  username: string;
  email: string;
  password: string;
  profilePic?: string;
}

export default function Settings() {
  const [file, setFile] = useState<File | null>(null);
  const { user, dispatch } = useContext(Context);

  const [username, setUsername] = useState(user.username);
  const [email, setEmail] = useState(user.email);
  const [password, setPassword] = useState("");
  const [success, setSuccess] = useState(false);

  const PF = "http://localhost:5000/images/"

  const handleSetFile = (files: FileList | null ) =>{
    if (files && files.length){
      setFile(files[0]);
    }
  }

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    dispatch({ type: "UPDATE_START" });
    const updatedUser = {
      userId: user._id,
      username,
      email,
      password,
      profilePic: ''
    };
    if (file) {
      const data = new FormData();
      const filename = Date.now() + file.name;
      data.append("name", filename);
      data.append("file", file);
      updatedUser.profilePic = filename;
      try {
        await Api.Files.upload(data);
      } catch (err) {}
    }
    try {
      const res = await axios.put("/users/" + user._id, updatedUser);
      setSuccess(true);
      dispatch({ type: "UPDATE_SUCCESS", payload: {user:res.data.user, token: res.data.token} });
    } catch (err) {
      dispatch({ type: "UPDATE_FAILURE" });
    }
  };
  return (
    <>
      <div className="settingsWrapper">
        <div className="settingsTitle">
          <span className="settingsUpdateTitle">Update Your Account</span>
        </div>
        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 1 }}>
        <label>Profile Picture</label>
          <div className="settingsPP">
            <div className="avatar-user">
              {
                (user.profilePic || file) && 
                  <img className="avatar-icon"
                    src={file ? URL.createObjectURL(file) : PF+user.profilePic}
                    alt=""
                  />
              }
            </div>
            <Button className="add-file-button" variant="contained" component="label" color="primary">
              {" "}
              <Add /> Upload avatar
              <input type="file" id="fileInput" name="fileInput" hidden onChange={(e) => handleSetFile(e.target.files)}/>
            </Button>
          </div>
          <TextField
                margin="normal"
                required
                fullWidth
                id="username"
                label="Username"
                name="username"
                autoFocus
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
              <TextField
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoFocus
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <TextField
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Update
              </Button>
            </Box>
            {success && (
              <span
                style={{ color: "green", textAlign: "center", marginTop: "20px" }}
              >
                Profile has been updated...
              </span>
            )}
      </div>
    </>
  );
}

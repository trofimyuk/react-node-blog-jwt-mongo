import { useContext, useState } from "react";
import "./write.css";
import { Context } from "../../context/Context";
import Api from "../../api";
import React from "react";
import { Box, Button, TextField } from "@mui/material";

export default function Write() {
  const [title, setTitle] = useState("");
  const [desc, setDesc] = useState("");
  const [file, setFile] = useState<File | null>(null);
  const { user } = useContext(Context);

  const handleSetFile = (files: FileList | null ) =>{
    if (files && files.length){
      setFile(files[0]);
    }
  }

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const newPost = {
      title,
      desc,
      photo: ''
    };
    if (file) {
      const data =new FormData();
      const filename = Date.now() + file.name;
      data.append("name", filename);
      data.append("file", file);
      newPost.photo = filename;
      try {
        await Api.Files.upload(data);
      } catch (err) {}
    }
    try {
      const res = await Api.Posts.post(newPost);
      window.location.replace("/post/" + res.data._id);
    } catch (err) {}
  };
  return (
    <div className="write">
      {file && (
        <img className="writeImg" src={URL.createObjectURL(file)} alt="" />
      )}
       <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 1 }}>
        <label>Post Picture</label>
          <div className="settingsPP">
              <label htmlFor="fileInput">
                <i className="writeIcon fas fa-plus"></i>
              </label>
              <input
                type="file"
                id="fileInput"
                style={{ display: "none" }}
                onChange={(e) => handleSetFile(e.target.files)}
              />
          </div>
          <TextField
                margin="normal"
                required
                fullWidth
                id="title"
                label="Title"
                placeholder="Title"
                name="title"
                autoFocus
                value={title}
                onChange={(e)=>setTitle(e.target.value)}
              />
              <TextField
                margin="normal"
                required
                fullWidth
                id="desc"
                label="Description"
                placeholder="Tell your story..."
                name="desc"
                autoFocus
                value={desc}
                minRows={3}
                multiline
                onChange={(e)=>setDesc(e.target.value)}
              />              
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Publish
              </Button>
            </Box>
    </div>
  );
}

import React, { useContext, useEffect } from "react";
import { Context } from "./context/Context";
import Container from '@mui/material/Container';
import Router from './routes/routes';
import Api from "./api";

const App = () => {

  const { token, dispatch } = useContext(Context);

  useEffect(() => {
    const loginWithToken = async () => {
      dispatch({ type: "LOGIN_START" });
      try {
        const res = await Api.Auth.loginWithToken();
        if (res.status === 200){
          dispatch({ type: "LOGIN_SUCCESS", payload: {user:res.data.user, token: res.data.token} });
        }else{
          dispatch({ type: "LOGIN_FAILURE" });
        }
      } catch (err) {
        dispatch({ type: "LOGIN_FAILURE" });
      }
    };
    if (token){
      loginWithToken();
    }
  }, []);


  return (
    <Container maxWidth="lg">
      <Router/>
    </Container>
  );
}

export default App;

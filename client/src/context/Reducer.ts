const Reducer = (state: any, action: any) => {
  switch (action.type) {
    case "LOGIN_START":
      console.log('LOGIN_START', state)
      return {
        ...state,
        user: null,
        isFetching: true,
        isAuth: false,
        error: false,
      };
    case "LOGIN_SUCCESS":
      return {
        user: action.payload.user,
        token: action.payload.token,
        isFetching: false,
        isAuth: true,
        error: false,
      };
    case "LOGIN_FAILURE":
      return {
        user: null,
        token: null,
        isFetching: false,
        isAuth: false,
        error: true,
      };
      case "UPDATE_START":
        return {
          ...state,
          isFetching:true,
        };
      case "UPDATE_SUCCESS":
        return {
          ...state,
          user: action.payload.user,
          token: action.payload.token,
          isFetching: false,
          error: false,
        };
      case "UPDATE_FAILURE":
        return {
          ...state,
          user: action.payload.user,
          token: action.payload.token,
          isFetching: false,
          error: true,
        };
    case "LOGOUT":
      return {
        user: null,
        token: null,
        isFetching: false,
        isAuth: false,
        error: false,
      };
    default:
      return state;
  }
};

export default Reducer;

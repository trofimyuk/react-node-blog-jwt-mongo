import React, { createContext, useEffect, useReducer } from "react";
import Reducer from "./Reducer";

export interface IContext {
  user: any;
  token: string | unknown;
  isFetching: boolean;
  error: boolean;
  dispatch: any;
  isAuth: boolean;
}

const INITIAL_STATE = {
  user: sessionStorage.getItem("user") ? JSON.parse(sessionStorage.getItem("user") || '{}') : null,
  token: sessionStorage.getItem("token"),
  isFetching: false,
  error: false,
  isAuth: false,
  dispatch: null,
};

console.log('1:',INITIAL_STATE);

export const Context = createContext<IContext>(INITIAL_STATE);

export const ContextProvider = ({ children } : any) => {
  const [state, dispatch] = useReducer(Reducer, INITIAL_STATE);

  useEffect(() => {
    if (!state.user){
      sessionStorage.removeItem("user")
    }else{
      sessionStorage.setItem("user", JSON.stringify(state.user));
    }
  }, [state.user]);

  useEffect(() => {
    if (!state.token){
      sessionStorage.removeItem("token")
    }else{
      sessionStorage.setItem("token", state.token);
    }
  }, [state.token]);

  return (
    <Context.Provider
      value={{
        user: state.user,
        token: state.token,
        isFetching: state.isFetching,
        error: state.error,
        isAuth: state.isAuth,
        dispatch,
      }}>{children}</Context.Provider>
  );
};
